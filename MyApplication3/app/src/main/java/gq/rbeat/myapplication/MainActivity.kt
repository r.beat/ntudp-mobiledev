package gq.rbeat.myapplication

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun corr(view: View) {
        val toastCorrect = Toast.makeText(this, "Yes, you're correct!", Toast.LENGTH_SHORT)
        toastCorrect.show()
    }
    fun incorr(view: View) {
        val toastIncorrect = Toast.makeText(this, "Sorry, you're wrong. Try again!", Toast.LENGTH_SHORT)
        toastIncorrect.show()
    }
}