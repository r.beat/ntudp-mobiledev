package gq.rbeat.myapplication

import androidx.appcompat.app.AppCompatActivity import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView import android.widget.Toast

class MainActivity : AppCompatActivity() {
    // Declaring interface buttons in Kotlin
    private lateinit var trueButton  : Button
    private lateinit var falseButton : Button
    private lateinit var nextButton  : Button
    private lateinit var questionTV  : TextView

    // Creating questions array
    private val q = listOf(
        Question(R.string.q1, true ),
        Question(R.string.q2, true ),
        Question(R.string.q3, false),
        Question(R.string.q4, true ),
        Question(R.string.q5, false),
        Question(R.string.q6, true )
    )

    // Setting default index in q's array
    private var curr = 0

    // Function to check the provided answer
    private fun checkAnswer(userAnswer: Boolean) {
        val correctAnswer = q[curr].answer
        val messageResId = if (userAnswer == correctAnswer) {
            R.string.correct_toast
        } else {
            R.string.incorrect_toast
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        // Creating the instance
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Tieing the buttons to Kotlin
        trueButton  = findViewById(R.id.true_button)
        falseButton = findViewById(R.id.false_button)
        nextButton  = findViewById(R.id.next_button)
        questionTV  = findViewById(R.id.question_text_view)

        // Setting Click Listeners
        trueButton.setOnClickListener  {_: View -> checkAnswer(true )}
        falseButton.setOnClickListener {_: View -> checkAnswer(false)}
        nextButton.setOnClickListener  {
            curr = (curr + 1) % q.size
            val questionTextResId = q[curr].question
            questionTV.setText(questionTextResId)
        }

        // Piping the first question to the interface
        val questionTextResId = q[curr].question
        questionTV.setText(questionTextResId)

    }
}